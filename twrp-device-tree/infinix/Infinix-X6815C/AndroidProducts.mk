#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_Infinix-X6815C.mk

COMMON_LUNCH_CHOICES := \
    omni_Infinix-X6815C-user \
    omni_Infinix-X6815C-userdebug \
    omni_Infinix-X6815C-eng
